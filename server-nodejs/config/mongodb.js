const mongoose = require("mongoose");

const clientOptions = {
  useNewUrlParser: true,
  dbName: "blog",
};

exports.initMongoDbConnection = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URL, clientOptions);
    console.log("Connected");
  } catch (error) {
    console.log(error);
    throw error;
  }
};
