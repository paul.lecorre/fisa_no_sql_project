const redis = require("redis");

const redisOptions = {
  socket: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  },
  legacyMode: true,
};

const client = redis.createClient(redisOptions);

try {
  client.connect();
  console.log("Connected");
} catch (error) {
  console.log(error);
  throw error;
}

module.exports = client;
