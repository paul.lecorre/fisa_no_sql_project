const mongoose = require("mongoose");

const postSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  comments: {
    type: [
      {
        author: String,
        message: String,
      },
    ],
    default: [],
  },
  likers: {
    type: [String],
    default: [],
  },
});

const Post = mongoose.model("Post", postSchema);

async function sendPost(title, content, author) {
  const post = await Post.create({
    title: title,
    content: content,
    author: author,
  });
  console.log(post);
  return post;
}

async function getAllPosts() {
  return await Post.find().sort({ _id: -1 });
}

async function getPostById(id) {
  return await Post.findById(id);
}

async function likePost(id, user_id) {
  const post = await getPostById(id);
  console.log(post);
  console.log(id);
  if (post.likers.includes(user_id)) {
    post.likers = post.likers.filter((id) => id !== user_id);
  } else {
    post.likers.push(user_id);
  }
  return await post.save();
}

async function commentPost(id, user_id, message) {
  const post = await getPostById(id);
  post.comments.push({
    message: message,
    author: user_id,
  });
  return await post.save();
}

module.exports = {
  sendPost,
  getAllPosts,
  likePost,
  commentPost,
};
