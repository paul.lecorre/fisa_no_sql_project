const mongoose = require("mongoose");
const redis = require("../config/redis_client.js");
const argon2 = require("argon2");
const crypto = require("crypto");

const AccountSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const Account = mongoose.model("Account", AccountSchema);

// Account model functions
async function getUserById(id) {
  return await Account.findById(id);
}

async function getUserByUsername(username) {
  return await Account.findOne({ username: username });
}

async function createAccount(username, password) {
  if (await getUserByUsername(username)) {
    return false;
  }
  const salt = generateSalt();
  hashedPassword = await hashPasswordWithSalt(password, salt);

  const user = await Account.create({
    username: username,
    password: hashedPassword,
  });

  if (!user) {
    return false;
  }

  redis.set("salt:" + user._id, salt);
  redis.save();

  return user;
}

async function login(username, password) {
  const user = await getUserByUsername(username);
  if (!user) {
    return false;
  }
  if (await checkPassword(password, user)) {
    return user;
  }
  return false;
}

// Password utils functions
async function checkPassword(password, user) {
  return new Promise((resolve, reject) => {
    redis.get("salt:" + user._id, async (err, salt) => {
      if (err) {
        reject(err);
        return;
      }

      try {
        resolve(await verifyPasswordWithSalt(password, salt, user.password));
      } catch (error) {
        reject(error);
      }
    });
  });
}

function generateSalt(length = 16) {
  return crypto.randomBytes(length).toString("hex");
}

async function hashPasswordWithSalt(password, salt) {
  try {
    return await argon2.hash(password + salt);
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function verifyPasswordWithSalt(password, salt, hash) {
  try {
    return await argon2.verify(hash, password + salt);
  } catch (err) {
    console.error(err);
    throw err;
  }
}

module.exports = {
  getUserById,
  getUserByUsername,
  createAccount,
  login,
};
