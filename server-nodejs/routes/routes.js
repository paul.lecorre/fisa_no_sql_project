const express = require("express");
const {
  showMainPage,
  showLogin,
  login,
  logout,
  showSignUp,
  signUp,
  sendPost,
  like,
  comment,
} = require("../controllers/controllers");
const router = express.Router();

// Define routes
router.get("/login", showLogin);
router.post("/login", login);
router.get("/signup", showSignUp);
router.post("/signup", signUp);
router.use("/logout", logout);
router.use("/like/:id", like);
router.use("/comment/:id", comment);
router.get("/", showMainPage);
router.post("/", sendPost);
router.use("*", (req, res) => res.redirect("/"));

// Export router
module.exports = router;
