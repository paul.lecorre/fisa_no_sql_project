const Account = require("../models/accounts.js");
const Post = require("../models/posts.js");

async function showMainPage(req, res) {
  if (!isAuthenticated(req)) {
    return res.redirect("/login");
  }
  const user = await Account.getUserById(req.session.user_id);
  const posts = await Post.getAllPosts();
  for (let i = 0; i < posts.length; i++) {
    for (let j = 0; j < posts[i].comments.length; j++) {
      let name = await Account.getUserById(posts[i].comments[j].author);
      posts[i].comments[j].author = name.username;
    }
  }
  return res
    .status(200)
    .render("index", { username: user.username, posts: posts });
}

function showLogin(req, res) {
  if (isAuthenticated(req)) {
    return res.redirect("/");
  }
  if (req.query.account_created == 1) {
    return res.status(200).render("login", { retry: false, ok: true });
  }
  return res.status(200).render("login", { retry: false, ok: false });
}

function showSignUp(req, res) {
  if (isAuthenticated(req)) {
    return res.redirect("/");
  }
  return res.status(200).render("signup", { retry: false });
}

async function sendPost(req, res) {
  if (!isAuthenticated(req)) {
    return res.redirect("/login");
  }
  if (req.body.title != "" && req.body.content != "") {
    const user = await Account.getUserById(req.session.user_id);
    const post = await Post.sendPost(
      req.body.title,
      req.body.content,
      user.username
    );
    return res.status(200).redirect("/");
  }
  return res.status(400).redirect("/");
}

async function like(req, res) {
  const postId = req.params.id;
  if (!isAuthenticated(req)) {
    return res.redirect("/login");
  }
  var post = await Post.likePost(postId, req.session.user_id);

  return res.status(200).redirect("/");
}

async function comment(req, res) {
  const postId = req.params.id;
  if (!isAuthenticated(req)) {
    return res.redirect("/login");
  }
  if (req.body.message != "") {
    var post = await Post.commentPost(
      postId,
      req.session.user_id,
      req.body.message
    );
    return res.status(200).redirect("/");
  }
  return res.status(400).redirect("/");
}

async function signUp(req, res) {
  if (req.body.username != "" && req.body.password != "") {
    if (await Account.createAccount(req.body.username, req.body.password)) {
      return res.status(201).redirect("login?account_created=1");
    } else {
      return res.status(400).render("signup", { retry: true });
    }
  } else {
    return res.status(400).render("signup", { retry: true });
  }
}

async function login(req, res) {
  if (req.body.username != "" && req.body.password != "") {
    const user = await Account.login(req.body.username, req.body.password);
    if (user) {
      req.session.user_id = user._id;
      return res.status(200).redirect("/");
    } else {
      return res.status(201).render("login", { retry: true, ok: false });
    }
  } else {
    return res.status(400).render("login", { retry: false, ok: false });
  }
}

function logout(req, res) {
  if (isAuthenticated(req)) {
    req.session.destroy();
  }
  return res.redirect("/login");
}

function isAuthenticated(req) {
  if (req.session.user_id) {
    return true;
  }
  return false;
}

function getUser(id) {
  let user = Account.getUserById(id);
  return user.username;
}

module.exports = {
  showMainPage,
  showLogin,
  showSignUp,
  login,
  signUp,
  logout,
  sendPost,
  like,
  comment,
};
