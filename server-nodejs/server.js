const express = require("express");
const session = require("express-session");
const redisStore = require("connect-redis")(session);
require("dotenv").config();

// Initialisation de la connexion aux DB
const redisClient = require("./config/redis_client");
const mongodb_cfg = require("./config/mongodb");
mongodb_cfg.initMongoDbConnection();

// Initialisation du serveur
require("ejs");
const router = require("./routes/routes.js");
const app = express();

// Configuration de la session
app.use(
  session({
    secret: "secret",
    store: new redisStore({
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      client: redisClient,
      prefix: "session:",
    }),
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false },
  })
);

// Configuration du serveur
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.use(router);

// Lancement du serveur
app.listen(process.env.LISTENING_PORT, () => {
  console.log(
    `Server running at http://localhost:${process.env.LISTENING_PORT}/`
  );
});
