db.createUser({
  user: "blog-user",
  pwd: "bl0g-p4ssw0rd",
  roles: [
    {
      role: "readWrite",
      db: "blog",
    },
  ],
});

db = new Mongo().getDB("blog");
db.createCollection("accounts");
db.createCollection("posts");
